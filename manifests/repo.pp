define yum::repo ($repo = $title, $ensure = 'file', $gpgkey = '', $reposet = 'default') {
    if $::yum::enabled {
	if $ensure == 'absent' {
	    file { "/etc/yum.repos.d/${repo}.repo":
		ensure => 'absent',
		notify => Exec['/usr/bin/yum clean metadata'],
	    }
	} else {
            $repofile = lookup("yum::repo::${repo}", { default_value => "${repo}.repo"})
	    file { "/etc/yum.repos.d/${repo}.repo":
		ensure => "${ensure}",
		owner => 'root',
		group => 'root',
		mode => '0644',
                source => "${::yum::repofilestore}/etc/yum.repos.d/${repofile}",
		notify => Exec['/usr/bin/yum clean metadata'],
	    }
	}
	if $gpgkey != '' {
	    exec {"rpmgpgimport ${gpgkey}" :
		command => "/usr/bin/rpm --import /etc/pki/rpm-gpg/${gpgkey}",
		refreshonly => true,
	    }
	    file { "/etc/pki/rpm-gpg/${gpgkey}" :
		ensure => file,
		owner => 'root',
		group => 'root',
		mode => '0644',
		source => ["${::yum::repofilestore}/etc/pki/rpm-gpg/${gpgkey}"],
		notify => Exec["rpmgpgimport ${gpgkey}"],
	    }
	}
    }
}
