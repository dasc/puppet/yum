define yum::gpgkey ($gpgkey = $title, $ensure = 'file', $reposet = 'default') {
    if $::yum::enabled {
	exec {"rpmgpgimport ${gpgkey}" :
	    command => "/usr/bin/rpm --import /etc/pki/rpm-gpg/${gpgkey}",
	    refreshonly => true,
	}
	file { "/etc/pki/rpm-gpg/${gpgkey}" :
	    ensure => "${ensure}",
	    owner => 'root',
	    group => 'root',
	    mode => '0644',
	    source => ["${::yum::repofilestore}/etc/pki/rpm-gpg/${gpgkey}"],
	    notify => Exec["rpmgpgimport ${gpgkey}"],
	}
    }
}
