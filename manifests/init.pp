class yum (
    Boolean $enabled = false,
    Boolean $cron_enable,
    String $cron_ensure,
    Boolean $cron_hourly_download_updates,
    Boolean $cron_hourly_apply_updates,
    Integer $cron_hourly_sleep,
    String $cron_hourly_exclude,
    String $cron_hourly_debuglevel,
    Boolean $cron_daily_download_updates,
    Boolean $cron_daily_apply_updates,
    Integer $cron_daily_sleep,
    String $cron_daily_exclude,
    String $cron_daily_debuglevel,
    String $repofilestore = undef,
    Optional[String] $proxy = undef,
    String $conffile_location = '/etc/yum.conf',
) {
    if $enabled {
	exec {'/usr/bin/yum clean metadata':
	    command => '/usr/bin/yum --enablerepo \* clean metadata',
	    refreshonly => true,
	} -> Package <| provider != 'rpm' |>

	file {'yum.conf':
	    path => $conffile_location,
	    ensure => present,
	    owner => 'root',
	    group => 'root',
	    mode => '0644',
	    content => epp('yum/yum.conf.epp', {
		'proxy' => $proxy,
	    }),
	}

	service {'yum-cron':
	    enable => $cron_enable,
	    ensure => $cron_ensure,
	}

	package {'yum-cron': ensure => installed, }
	file {'/etc/yum/yum-cron.conf':
	    ensure => present,
	    owner => 'root',
	    group => 'root',
	    mode => '0644',
	    content => epp('yum/yum-cron.conf.epp', {
		'cron_download_updates' => $cron_daily_download_updates,
		'cron_apply_updates' => $cron_daily_apply_updates,
		'cron_sleep' => $cron_daily_sleep,
		'cron_exclude' => $cron_daily_exclude,
		'cron_debuglevel' => $cron_daily_debuglevel,
	    }),
	}

	file {'/etc/yum/yum-cron-hourly.conf':
	    ensure => present,
	    owner => 'root',
	    group => 'root',
	    mode => '0644',
	    content => epp('yum/yum-cron.conf.epp', {
		'cron_download_updates' => $cron_hourly_download_updates,
		'cron_apply_updates' => $cron_hourly_apply_updates,
		'cron_sleep' => $cron_hourly_sleep,
		'cron_exclude' => $cron_hourly_exclude,
		'cron_debuglevel' => $cron_hourly_debuglevel,
	    }),
	}
    }

    Yum::Repo <| |> -> Package <| provider != 'rpm' |>
}
